//
//  NewViewController.m
//  Tarea1
//
//  Created by Jose Mendoza on 6/30/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "NewViewController.h"
#import "RootViewController.h"


@interface NewViewController ()

@end

@implementation NewViewController

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    
    [self setDefault];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark utilitary methods

- (void)setDefault{
    self.txtName.delegate = self;
    self.txtBrand.delegate = self;
    self.txtYear.delegate = self;
    self.txtAmount.delegate = self;
    self.txtDescription.delegate = self;
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(Boolean) stringIsNumeric: (NSString*)input
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *number             = [formatter numberFromString:input];
    formatter                    = nil;
    
    return !!number;
}

- (IBAction)doSave:(id)sender {

    [self validateInfo];
}


#pragma mark logic methods

- (void) validateInfo {
    NSString* error_message = @"";
    
    if ([self.txtName.text isEqual:@""]){
        error_message = @"Debe de indicar el nombre del cliente";
    }
    
    if ([self.txtBrand.text isEqual:@""]){
        error_message = @"Debe de indicar la marca del vehiculo";
    }
    
    if ([self.txtDescription.text isEqual:@""]){
        error_message = @"Debe de indicar una descripcion del trabajo";
    }
    
    if ([self.txtAmount isEqual:@""] || [self.txtAmount isEqual:@"0"] || [self.txtAmount isEqual:@"."]){
        error_message = @"Debe de indicar un monto valido";
    }
    
    if ([self.txtYear isEqual:@""] || [self.txtYear isEqual:@"0"]){
        error_message = @"Debe de indicar un año valido";
    }
    
    
    if ([error_message isEqual:@""] == false){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información Incorrecta"
                                                    message:error_message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    }else {
        [self save];
    }
    
}



- (void)save{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newWork = [NSEntityDescription insertNewObjectForEntityForName:@"Work" inManagedObjectContext:context];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *amount = [f numberFromString:self.txtAmount.text];
    int year = [self.txtYear.text intValue];

    
    [newWork setValue:[NSDate date] forKey:@"wdate"];
    [newWork setValue:self.txtBrand.text forKey:@"wbrand"];
    [newWork setValue:self.txtDescription.text forKey:@"wdescription"];
    [newWork setValue:self.txtName.text forKey:@"wname"];
    [newWork setValue:[NSNumber numberWithInt:year]  forKey:@"wyear"];
    [newWork setValue:amount forKey:@"wamount"];
    
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"No se pudo Salvar! %@ %@", error, [error localizedDescription]);
    }
    else{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self goToRootViewController];
    }

}


-(void)goToRootViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
