//
//  ListViewController.m
//  Tarea1
//
//  Created by Jose Mendoza on 7/4/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "ListViewController.h"
#import "WorkTableViewCell.h"
@import CoreData;

@interface ListViewController ()
@property (strong) NSMutableArray *works;


@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Work"];
    self.works = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.works.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    WorkTableViewCell *cell = (WorkTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
   
    // Configure the cell...
    NSManagedObject *work = [self.works objectAtIndex:indexPath.row];
    //[cell.textLabel setText:[NSString stringWithFormat:@"%@ %@", [device valueForKey:@"name"], [device valueForKey:@"version"]]];
    //[cell.detailTextLabel setText:[device valueForKey:@"company"]];
    
    
    [cell.lblAmount      setText:[NSString stringWithFormat:@"Monto: %@",[work valueForKey:@"wamount"]]];
    [cell.lblBrand     setText:[NSString stringWithFormat:@"Marca: %@",[work valueForKey:@"wbrand"]]];
    [cell.lblDate     setText:[NSString stringWithFormat:@"Fecha: %@",[work valueForKey:@"wdate"]]];
    [cell.lblDescription setText:[NSString stringWithFormat:@"Descripción: %@",[work valueForKey:@"wdescription"]]];
    [cell.lblName setText:[NSString stringWithFormat:@"Nombre: %@",[work valueForKey:@"wname"]]];
    [cell.lblYear setText:[NSString stringWithFormat:@"Año: %@",[work valueForKey:@"wyear"]]];

    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
