//
//  NewViewController.h
//  Tarea1
//
//  Created by Jose Mendoza on 6/30/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;

@interface NewViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

-(void) validateInfo;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtBrand;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
